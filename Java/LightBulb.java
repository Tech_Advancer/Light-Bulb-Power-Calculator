public class LightBulb {
    
    // cost to buy Bulbs (per bulb), watts (per bulb), lifeSpan (in hours)
    private double cost, watts;
    private int lifeSpan;
    
    // Empty Constructor
    public LightBulb(){}
    
    // Full Constructor
    public LightBulb(double cost, double watts, int lifeSpan){
        this.cost = cost;
        this.watts = watts;
        this.lifeSpan = lifeSpan;
    }
    
    public void setCost(double cost){
        this.cost = cost;
    }
    
    public void setWatts(double watts){
        this.watts = watts;
    }
    
    public void setLifeSpan(int lifeSpan){
        this.lifeSpan = lifeSpan;
    }
    
    public double getCost(){
        return this.cost;
    }
    
    public double getWatts(){
        return this.watts;
    }
    
    public int getLifeSpan(){
        return this.lifeSpan;
    }
} 

