import java.text.NumberFormat;
import java.util.Locale;

public class Driver {
    public static void main(String[] args) {

        // The cost per watt, the number of hours the light is on, and the number of bulbs
        double cost = 0.13;
        int hours = 24, number = 10;

        // Initialize a new LightBulbCalculator
        LightBulbCalculator lbc = new LightBulbCalculator(cost, hours, number);

        // Initialize two new LightBulbs with the cost per bulb, watts per bulb, and the lifespan in hours
        LightBulb one = new LightBulb(5.42, 7, 25000); // GE LED
        LightBulb two = new LightBulb(1.57, 15, 2500); // Best CFL

        // Setup our currency formatter so we can display prices neatly
        Locale locale = new Locale("en", "US");
        NumberFormat currency = NumberFormat.getCurrencyInstance(locale);

        int i = 0;

        while(i<10){
            i = i + 1;

            switch(lbc.compareBulbs(one, two, i)){ // Compare our bulbs, -1 = one is cheaper, 0 = no difference, 1 = two is cheaper
                // getDifference returns the difference for the given year, in this case i
                case -1: System.out.println("Bulb 1 is cheaper by " + currency.format(lbc.getDifference(one, two, i))); break;
                case 0: System.out.println("No Difference."); break;
                case 1: System.out.println("Bulb 2 is cheaper by " + currency.format(lbc.getDifference(one, two, i))); break;
                default: System.out.println("Error!"); break;
            }
        }

	// TODO: Add GUI

}


