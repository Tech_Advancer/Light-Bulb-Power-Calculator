public class LightBulbCalculator {
    
    // cost of electricity (in kw/h), hours of use (per day), and number of bulbs
    private double cost;
    private int hours, number;
    
    // Empty Constructor
    public LightBulbCalculator(){}
    
    // Full Constructor
    public LightBulbCalculator(double cost, int hours, int number){
        this.cost = cost;
        this.hours = hours;
        this.number = number;
    }
    
    public double getReplacementCost(LightBulb bulb, int year){        
        return (Math.floor((year*365*this.hours) / (bulb.getLifeSpan() + 24)) * (bulb.getCost()*number))
                + (bulb.getCost()*number);
    }
    
    public double getPowerCost(LightBulb bulb, int year){
        return ((bulb.getWatts() * number * hours) / 1000) * number * cost * ( year * 365 );
    }
    
    public double getTotalCost(LightBulb bulb, int year){
        return this.getReplacementCost(bulb, year) + this.getPowerCost(bulb, year);
    }

    public int compareBulbs(LightBulb one, LightBulb two, int year){
        /*
        Returns -1 if Bulb 1 is Cheaper.
        Returns 0 if they are the same.
        Returns 1 if Bulb 2 is Cheaper.
        */
        
        if( this.getTotalCost(one, year) < this.getTotalCost(two, year) ){ return -1; }
        if( this.getTotalCost(one, year) == this.getTotalCost(two, year) ){ return 0; }
        if( this.getTotalCost(one, year) > this.getTotalCost(two, year) ){ return 1; }
        
        return -5;
    }
    
    public double getDifference(LightBulb one, LightBulb two, int year){
        /*
            Returns the cost difference of the more expensive minus the cheaper.
        */
        
        if(this.compareBulbs(one, two, year) == -1){ return this.getTotalCost(two, year) - this.getTotalCost(one,year); }
        if(this.compareBulbs(one, two, year) == 0){ return 0; }
        if(this.compareBulbs(one, two, year) == 1){ return this.getTotalCost(one, year) - this.getTotalCost(two,year); }
        
        return 0.0;
    }
    
}
 

