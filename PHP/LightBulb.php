<?php
/////////////////////////////////////////////////////////////////
//
// LightBulb.php
// Sets and holds data about a single lightbulb.
//
/////////////////////////////////////////////////////////////////

class LightBulb{

	// cost to buy Bulbs (per bulb), watts (per bulb), lifeSpan (in hours)
	private $cost, $watts, $lifeSpan;

	public function __construct($cost, $watts, $lifeSpan){
		$this->cost = (double)$cost;
 		$this->watts = (double)$watts;
		$this->lifeSpan = (int)$lifeSpan;
	}

	public function setCost($cost){
		$this->cost = (double)$cost;
	}

	public function setWatts($watts){
		$this->watts = (double)$watts;
	}

	public function setLifeSpan($lifeSpan){
		$this->lifeSpan = (int)$lifeSpan;
	}

	public function getCost(){
	        return (double)$this->cost;
	}

	public function getWatts(){
	        return (double)$this->watts;
	}

	public function getLifeSpan(){
	        return (int)$this->lifeSpan;
	}

}
