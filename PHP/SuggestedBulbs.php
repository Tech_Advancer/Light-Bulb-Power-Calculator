<?php
/////////////////////////////////////////////////////////////////
//
// suggestedBulbs.php
// Loads in the suggested bulbs as an array.
//
/////////////////////////////////////////////////////////////////


$suggestedBulbs = [];

/*
	Values for $suggestedBulbs:
	___________________________
	name (string)
	availability (string)
	cost (double)
	watts (double)
	lifespan (int)
	note (string)
*/


// These are hard-coded right now. In the future they should be loaded in from a common file outside of the PHP directory.
// That way all projects can use it.

$suggestedOne = array(
"name" => 'UT LED 40W Equivilant Bulb 2-CT',
"availability" => 'Lowe\'s Hardware',
"url" => 'http://www.lowes.com/pd_740059-43921-YGA03A41-A19-5.5W-83_1z0vvnnZ1z10fgwZ1z10ht5Z1z140qi__?productId=50423210&pl=1',
"cost" => 1.49,
"watts" => 5.5,
"lifespan" => 5000,
"note" => 'Warning: I haven\'t tested this bulb! Also note that the lifespan is lower and it has mixed reviews online!'
);

$suggestedTwo = array(
"name" => 'OSRAM LED (Not available online)',
"availability" => 'Lowe\'s Hardware',
"url" => '',
"cost" => 6.98,
"watts" => 6,
"lifespan" => 25000,
"note" => 'Warning: I haven\'t tested this bulb!'
);

$suggestedThree = array(
"name" => 'Great Value LED',
"availability" => 'Walmart',
"url" => 'http://www.walmart.com/ip/Great-Value-GVRLAO727D-Great-Value-LED-7W-Omni-Directional-A19-Dimmable-Light-Bulb/38591387',
"cost" => 4.84,
"watts" => 7,
"lifespan" => 24900,
"note" => 'Warning: I haven\'t tested this bulb! OSRAM LED becomes cheaper than this one after 17 years.'
);

$suggestedFour = array(
"name" => 'GE LED 40W @ 7W',
"availability" => 'Target',
"url" => 'http://www.target.com/p/ge-led-40-watt-light-bulb-2-pack-soft-white/-/A-16762269?lnk=rec|pdp|related_prods_vv|pdpv1',
"cost" => 6.00,
"watts" => 7,
"lifespan" => 25000,
"note" => 'Also available at Kroger\'s, but slightly more expensive.'
);

$suggestedFive = array(
"name" => 'Utilitech 2-Pack Soft White CFL Bulb',
"availability" => 'Lowe\'s Hardware',
"url" => 'http://www.lowes.com/pd_20928-75774-LBP9T227K_0__?productId=50189167',
"cost" => 2.49,
"watts" => 9,
"lifespan" => 10000,
"note" => 'Just to compare with CFL\'s, here is the best CFL I could find for a 40 watt equivilant.'
);

$suggestedSix = array(
"name" => 'SYLVANIA 2-Pack Soft White Incandescent Light Bulbs',
"availability" => 'Lowe\'s Hardware',
"url" => "http://www.lowes.com/pd_76501-3-10015_0__?productId=1100567",
"cost" => 1.57,
"watts" => 15,
"lifespan" => 2500,
"note" => 'The lifespan isn\'t a typo! Just to compare, this is the BEST incandescent bulb I could find.'
);


$suggestedBulbs[] = $suggestedOne;
$suggestedBulbs[] = $suggestedTwo;
$suggestedBulbs[] = $suggestedThree;
$suggestedBulbs[] = $suggestedFour;
$suggestedBulbs[] = $suggestedFive;
$suggestedBulbs[] = $suggestedSix;


?>
