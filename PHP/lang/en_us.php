<?php
///////////////////////////////////////////////////////////////////////////////////
//
//  en_us.php
//  Sets up all the text for United States English. Other language files can be
//    made simply by copying the variable names below and changing the text
//    inside of the strings. Then save it as a new php file and update config.php.
//
///////////////////////////////////////////////////////////////////////////////////

abstract class lang
{
	// Error Messages:
	const genericError = 'Error Loading Website!<br><a href="./index.php">Reload</a>';
	const missingLightBulb = 'Error! LightBulb.php class file missing!';
	const missingLightBulbCalculator = 'Error! LightBulbCalculator.php class file missing!';
	const missingLightBulbCalculatorDraw = 'Error! LightBulbCalculatorDraw.php class file is missing!';
	const missingHTMLFile = 'Error! LightBulbCalculator.html HTML file missing!';
	const missingSuggestedBulbs = 'Error! SuggestedBulbs.php file missing!';

	// Other Text:
	const title = 'LightBulb Power Calculator';
	const fullTableMessage = '*All values below are added from the beginning to the listed year.';


	// Form Text:
	const generalInformation = 'General Information:';
	const numberOfBulbs = 'Number of Bulbs: ';
	const electricityCost = 'Electricity Cost: ';
	const hoursOfUse = 'Hours of Use per Day: ';
	const chart = 'Chart ';
	const years = ' years.';

	const bulbOne = 'Bulb One: ';
	const bulbTwo = 'Bulb Two: ';
	const costPerBulb = 'Cost Per Bulb: ';
	const wattPerBulb = 'Watt per Bulb: ';
	const lifespan = 'Bulb Lifespan (in hours): ';

	// Suggested Bulbs:
	const suggestedName = 'Name: ';
	const suggestedAvailableAt = 'Available at: ';
	const suggestedCostPerBulb = 'Cost per Bulb: ';
	const suggestedWattsPerBulb = 'Watts per Bulb: ';
	const suggestedLifespan = 'Lifespan: ';
	const suggestedHours = ' Hours';
	const suggestedAutofillOne = 'Autofill Bulb One';
	const suggestedAutofillTwo = 'Autofill Bulb Two';
	const suggestedNote = 'Note: ';

	// Side By Side:
		// Bulb One:
		const bulbOneInitialCost = 'Bulb 1 Initial Cost: ';
	        const bulbOneTotalWatts = 'Bulb 1 Total Watts: ';
	        const bulbOnePowerUsedPerDay = 'Bulb 1 Power Used per Day (kWh/Day): ';
	        const bulbOnePowerCostPerYear = 'Bulb 1 Power Cost per Year: ';

		// Bulb Two:
		const bulbTwoInitialCost = 'Bulb 2 Initial Cost: ';
                const bulbTwoTotalWatts = 'Bulb 2 Total Watts: ';
                const bulbTwoPowerUsedPerDay = 'Bulb 2 Power Used per Day (kWh/Day): ';
                const bulbTwoPowerCostPerYear = 'Bulb 2 Power Cost per Year: ';

	// Full Table:
		const year = 'Year';
		const difference = 'Difference';
		const noDifference = 'No Difference.';
			// Bulb One:
			const bulbOneReplacementCost = 'Bulb 1 Replacement Cost';
			const bulbOnePowerCost = 'Bulb 1 Power Cost';
			const bulbOneTotalCost = 'Bulb 1 Total Cost';
			const bulbOneCheaperBy = '<span class="bulbOne">Bulb 1</span> is cheaper by ';

			// Bulb Two:
                        const bulbTwoReplacementCost = 'Bulb 2 Replacement Cost';
                        const bulbTwoPowerCost = 'Bulb 2 Power Cost';
                        const bulbTwoTotalCost = 'Bulb 2 Total Cost';
			const bulbTwoCheaperBy = '<span class="bulbTwo">Bulb 2</span> is cheaper by ';
}
?>
