
<?php
/////////////////////////////////////////////////////////////////
//
// LightBulbCalculator.php
// Does the calculations.
//
/////////////////////////////////////////////////////////////////

class LightBulbCalculator{

	// cost of electricity (in kw/h), hours of use (per day), and number of bulbs
	private $cost, $hours, $number;

	public function __construct($cost, $hours, $number){
		$this->cost = (double)$cost;
		$this->hours = (int)$hours;
		$this->number = (int)$number;
	}

	public function getNumberOfBulbs(){
		return (int)$this->number;
	}

	public function getHoursOfUse(){
		return (int)$this->hours;
	}

	public function getCostOfPower(){
		return (double)$this->cost;
	}

	public function getReplacementCost($bulb, $year){
		return (double)(floor(((int)$year*365*$this->hours) / ($bulb->getLifeSpan() + 24)) * ($bulb->getCost()*$this->number)) + ($bulb->getCost()*$this->number);
	}

	public function getPowerCost($bulb, $year){
		return (double)(($bulb->getWatts() * $this->number * $this->hours) / 1000) * $this->number * $this->cost * ( (int)$year * 365 );
	}

	public function getTotalCost($bulb, $year){
		return (double)$this->getReplacementCost($bulb, (int)$year) + $this->getPowerCost($bulb, (int)$year);
	}

	public function compareBulbs($bulbOne, $bulbTwo, $year){
		/*
		Returns -1 if Bulb 1 is Cheaper.
		Returns 0 if they are the same.
		Returns 1 if Bulb 2 is Cheaper.
		*/

		if( $this->getTotalCost($bulbOne, (int)$year) < $this->getTotalCost($bulbTwo, (int)$year) ){ return -1; }
		if( $this->getTotalCost($bulbOne, (int)$year) == $this->getTotalCost($bulbTwo, (int)$year) ){ return 0; }
		if( $this->getTotalCost($bulbOne, (int)$year) > $this->getTotalCost($bulbTwo, (int)$year) ){ return 1; }

		return -5;
	}

	public function getDifference($bulbOne, $bulbTwo, $year){
		/*
		Returns the cost difference of the more expensive minus the cheaper.
		*/

		if($this->compareBulbs($bulbOne, $bulbTwo, (int)$year) == -1){ return (double)$this->getTotalCost($bulbTwo, (int)$year) - $this->getTotalCost($bulbOne,(int)$year); }
		if($this->compareBulbs($bulbOne, $bulbTwo, (int)$year) == 0){ return 0.0; }
		if($this->compareBulbs($bulbOne, $bulbTwo, (int)$year) == 1){ return (double)$this->getTotalCost($bulbOne, (int)$year) - $this->getTotalCost($bulbTwo,(int)$year); }

		return 0.0;
	}


}

?>

