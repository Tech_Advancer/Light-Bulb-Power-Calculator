<?php

// Get the root directory that this file is placed in.
$root = getcwd();

// Check that the configuration file exists
if(!file_exists('config.php')){
	die('config.php file missing!');
}

// Include the configuration file or die trying
if(!include_once('config.php')){
	die('Missing config.php file!');
}

// IF force https is on in the configuration file
if(@($_SERVER['HTTPS'] != 'on') && ($forceHttps) && (!headers_sent())){
	// Force a redirect to https
	header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
	exit();
}

// IF we are using https (set in configuration file), force session cookies to use it!
if($forceHttps){
	ini_set('session.cookie_secure',1);
}

// IF show errors is on in the configuration file
if($showErrors){
	error_reporting(E_ALL);
	ini_set('display_errors',1);
}

// Include the language file we selected in config.php:
if(!include_once('./lang/' . $lang)){
        die('Missing language file.');
}

// Include our bulb class. It stores all of the data for bulbs.
if(!include_once('./LightBulb.php')){
	die(lang::missingLightBulb);
}

// Include our calculator class. It does all of the math.
if(!include_once('./LightBulbCalculator.php')){
        die(lang::missingLightBulbCalculator);
}

// Include our drawing class. It prints the results out nicely on the webpage.
if(!include_once('./LightBulbCalculatorDraw.php')){
        die(lang::missingLightBulbCalculatorDraw);
}

// Include our suggested bulbs. It gets the bulbs we suggest and puts it into an array.
if(!include_once('./SuggestedBulbs.php')){
        die(lang::missingSuggestedBulbs);
}


if( (isset($_POST['submit'])) && (isset($_POST['numberOfBulbs'])) && (isset($_POST['costOfPower'])) && (isset($_POST['timeUsed'])) && (isset($_POST['yearsToChart']))
&& (isset($_POST['bulbOneCost'])) && (isset($_POST['bulbOneWatts'])) && (isset($_POST['bulbOneLifespan']))
&& (isset($_POST['bulbTwoCost'])) && (isset($_POST['bulbTwoWatts'])) && (isset($_POST['bulbTwoLifespan'])) ){ // IF the form was submitted

        if(is_numeric($_POST['numberOfBulbs'])){ $numberOfBulbs = (int)$_POST['numberOfBulbs']; }
        if(is_numeric($_POST['costOfPower'])){ $costOfPower = (double)$_POST['costOfPower']; }
        if(is_numeric($_POST['timeUsed'])){ $timeUsed = (int)$_POST['timeUsed']; }
	if(is_numeric($_POST['yearsToChart'])){ $yearsToChart = (int)$_POST['yearsToChart']; }

	// Setup our calculator class. It does all of the math.
	$calculator = new LightBulbCalculator( $costOfPower, $timeUsed, $numberOfBulbs );

        if(is_numeric($_POST['bulbOneCost'])){ $bulbOneCost = (double)$_POST['bulbOneCost']; }
        if(is_numeric($_POST['bulbOneWatts'])){ $bulbOneWatts = (double)$_POST['bulbOneWatts']; }
        if(is_numeric($_POST['bulbOneLifespan'])){ $bulbOneLifespan = (int)$_POST['bulbOneLifespan']; }

	// Setup our bulb class. This one stores all of the data for bulb one.
	$bulbOne = new LightBulb( $bulbOneCost, $bulbOneWatts, $bulbOneLifespan );

        if(is_numeric($_POST['bulbTwoCost'])){ $bulbTwoCost = (double)$_POST['bulbTwoCost']; }
        if(is_numeric($_POST['bulbTwoWatts'])){ $bulbTwoWatts = (double)$_POST['bulbTwoWatts']; }
        if(is_numeric($_POST['bulbTwoLifespan'])){ $bulbTwoLifespan = (int)$_POST['bulbTwoLifespan']; }

	// Setup our bulb class. This one stores all of the data for bulb two.
	$bulbTwo = new LightBulb( $bulbTwoCost, $bulbTwoWatts, $bulbTwoLifespan );

	// Setup our drawing class. It prints the results out nicely on the webpage.
        $draw = new LightBulbCalculatorDraw( $calculator, $bulbOne, $bulbTwo, $yearsToChart, $moneyFormat );

	$submitted = true;
}else{ // ELSE IF the form was submitted

	// Setup our drawing class. It prints the results out nicely on the webpage.
	// Even though nothing has been submitted, we still need it for the suggested bulbs
        $draw = new LightBulbCalculatorDraw( null, null, null, null, $moneyFormat );

	$submitted = false;
	$numberOfBulbs = 1;
	$costOfPower = 0.13;
	$timeUsed = 6;
	$yearsToChart = 10;
	$bulbOneCost = 1;
	$bulbOneWatts = 1.0;
	$bulbOneLifespan = 2400;
	$bulbTwoCost = 1;
	$bulbTwoWatts = 1.0;
	$bulbTwoLifespan = 2400;
} // END ELSE IF the form was submitted


// Include our html file. It lays out the formatting along with
//   the css file it pulls in.
if(!include_once('./LightBulbCalculator.html')){
        die(lang::missingHTMLFile);
}

?>
