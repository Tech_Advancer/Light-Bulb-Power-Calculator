<?php
///////////////////////////////////////////////////////////////////////////////////
//
//  config.php
//  Sets configuration variables. Change these to whatever you need.
//
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
//
//  Settings:
//
//  Set force_https to true if you want to force it to use a SSL
//  certficate to encrypt data. If you have https setup for your host,
//  setting this to 'true' will increase your security.
//
//  Example:   $forceHttps = false;
                $forceHttps = false;
//
//  Set language file to use. Language files are in (root)/lang/*:
//
//  Example:	$lang = 'es_mx.php';
                $lang = 'en_us.php';
//
//  Force php to show error messages for debugging. The default is false.
//
//  Example:	$showErrors = false;
		$showErrors = false;
//
//  Change how you want money to be displayed on the results page. %i is the
//  symbol for the actual money value. You can add to it by placing a symbol
//  like a dollar sign in front to show dollars.
//
//  Example 	$moneyFormat = '$ %i';
		$moneyFormat = '%i';
//
///////////////////////////////////////////////////////////////////////////////////
?>
