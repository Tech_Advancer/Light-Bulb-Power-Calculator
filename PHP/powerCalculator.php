<html>
<head>
<title>Light Bulb Power Calculator</title>
<style type="text/css">
tr,td{
border: 1px solid black;
}

a{
text-decoration: none;
}
a:hover{
text-decoration: underline;
}
</style>
</head>
<body>


<?php

$submitted = false;

function moneyFormat($format, $number)
{
/*
Formats money as US Dollars regardless of the type of server you are using.
This function is slightly modified code from a comment by Rafael M. Salvioni on:
https://secure.php.net/manual/en/function.money-format.php
 */

	setlocale(LC_MONETARY, 'en_US'); //just force the locale
	if(function_exists('money_format')){
		return money_format($format, $number);
	}else{
	    $regex  = '/%((?:[\^!\-]|\+|\(|\=.)*)([0-9]+)?'.'(?:#([0-9]+))?(?:\.([0-9]+))?([in%])/';
	    $locale = localeconv();
	    preg_match_all($regex, $format, $matches, PREG_SET_ORDER);
	    foreach ($matches as $fmatch) {
	        $value = floatval($number);
	        $flags = array(
	            'fillchar'  => preg_match('/\=(.)/', $fmatch[1], $match) ?
	                           $match[1] : ' ',
	            'nogroup'   => preg_match('/\^/', $fmatch[1]) > 0,
	            'usesignal' => preg_match('/\+|\(/', $fmatch[1], $match) ?
	                           $match[0] : '+',
	            'nosimbol'  => preg_match('/\!/', $fmatch[1]) > 0,
	            'isleft'    => preg_match('/\-/', $fmatch[1]) > 0
	        );
	        $width      = trim($fmatch[2]) ? (int)$fmatch[2] : 0;
	        $left       = trim($fmatch[3]) ? (int)$fmatch[3] : 0;
	        $right      = trim($fmatch[4]) ? (int)$fmatch[4] : $locale['int_frac_digits'];
	        $conversion = $fmatch[5];
	        $positive = true;
	        if ($value < 0) {
	            $positive = false;
	            $value  *= -1;
	        }
	        $letter = $positive ? 'p' : 'n';
	        $prefix = $suffix = $cprefix = $csuffix = $signal = '';
	        $signal = $positive ? $locale['positive_sign'] : $locale['negative_sign'];
	        switch (true) {
	            case $locale["{$letter}_sign_posn"] == 1 && $flags['usesignal'] == '+':
	                $prefix = $signal;
	                break;
	            case $locale["{$letter}_sign_posn"] == 2 && $flags['usesignal'] == '+':
	                $suffix = $signal;
	                break;
	            case $locale["{$letter}_sign_posn"] == 3 && $flags['usesignal'] == '+':
	                $cprefix = $signal;
	                break;
	            case $locale["{$letter}_sign_posn"] == 4 && $flags['usesignal'] == '+':
	                $csuffix = $signal;
	                break;
	            case $flags['usesignal'] == '(':
	            case $locale["{$letter}_sign_posn"] == 0:
	                $prefix = '(';
	                $suffix = ')';
	                break;
	        }
	        if (!$flags['nosimbol']) {
	            $currency = $cprefix .
	                        ($conversion == 'i' ? $locale['int_curr_symbol'] : $locale['currency_symbol']) .
	                        $csuffix;
	        } else {
	            $currency = '';
	        }
	        $space  = $locale["{$letter}_sep_by_space"] ? ' ' : '';
	        $value = number_format($value, $right, $locale['mon_decimal_point'],
	                 $flags['nogroup'] ? '' : $locale['mon_thousands_sep']);
	        $value = @explode($locale['mon_decimal_point'], $value);
	        $n = strlen($prefix) + strlen($currency) + strlen($value[0]);
	        if ($left > 0 && $left > $n) {
	            $value[0] = str_repeat($flags['fillchar'], $left - $n) . $value[0];
	        }
	        $value = implode($locale['mon_decimal_point'], $value);
	        if ($locale["{$letter}_cs_precedes"]) {
	            $value = $prefix . $currency . $space . $value . $suffix;
	        } else {
	            $value = $prefix . $value . $space . $currency . $suffix;
	        }
	        if ($width > 0) {
	            $value = str_pad($value, $width, $flags['fillchar'], $flags['isleft'] ?
	                     STR_PAD_RIGHT : STR_PAD_LEFT);
	        }
	        $format = str_replace($fmatch[0], $value, $format);
	    }
	    return $format;
	}
}

if( (isset($_POST['submit'])) && (isset($_POST['generalNumber'])) && (isset($_POST['generalCost'])) && (isset($_POST['generalUse'])) && (isset($_POST['generalYears']))
&& (isset($_POST['oneCost'])) && (isset($_POST['oneWatt'])) && (isset($_POST['oneLifespan']))
&& (isset($_POST['twoCost'])) && (isset($_POST['twoWatt'])) && (isset($_POST['twoLifespan'])) ){ // IF the form was submitted

        if(is_numeric($_POST['generalNumber'])){ $generalNumber = (int)$_POST['generalNumber']; }
        if(is_numeric($_POST['generalCost'])){ $generalCost = (double)$_POST['generalCost']; }
        if(is_numeric($_POST['generalUse'])){ $generalUse = (int)$_POST['generalUse']; }
	if(is_numeric($_POST['generalYears'])){ $generalYears = (int)$_POST['generalYears']; }

        if(is_numeric($_POST['oneCost'])){ $oneCost = (double)$_POST['oneCost']; }
        if(is_numeric($_POST['oneWatt'])){ $oneWatt = (double)$_POST['oneWatt']; }
        if(is_numeric($_POST['oneLifespan'])){ $oneLifespan = (int)$_POST['oneLifespan']; }

        if(is_numeric($_POST['twoCost'])){ $twoCost = (double)$_POST['twoCost']; }
        if(is_numeric($_POST['twoWatt'])){ $twoWatt = (double)$_POST['twoWatt']; }
        if(is_numeric($_POST['twoLifespan'])){ $twoLifespan = (int)$_POST['twoLifespan']; }

	$submitted = true;
}else{ // ELSE IF the form was submitted
	$generalNumber = 1;
	$generalCost = 0.13;
	$generalUse = 6;
	$generalYears = 10;
	$oneCost = 1;
	$oneWatt = 1.0;
	$oneLifespan = 2400;
	$twoCost = 1;
	$twoWatt = 1.0;
	$twoLifespan = 2400;
} // END ELSE IF the form was submitted
?>

<form action="<?php echo pathinfo(__FILE__, PATHINFO_FILENAME); ?>.php" method="POST">
<table style="width:100%;background-color:lightgray;">
<tr><td colspan="2" style="padding:1%;">

<h2>General Information:</h2>

<label>Number of Bulbs: <input type="number" min="1" max="2000000000" value="<?php echo $generalNumber; ?>" name="generalNumber"></label> <br>
<label>Electricity Cost: <input type="number" min="0.01" step="0.01" value="<?php echo $generalCost; ?>" name="generalCost"></label> <br>
<label>Hours of use per Day: <input type="number" min="1" max="24" value="<?php echo $generalUse; ?>" name="generalUse"></label> <br><br>

<label>Chart <input type="number" min="1" max="100" value="<?php echo $generalYears; ?>" name="generalYears"> years.</label><br><br>

</td></tr><tr><td style="padding:1%;">

<h2>Bulb One:  <span style="background-color: blue;">&nbsp;&nbsp;&nbsp;</span></h2>

<label>Cost Per Bulb: <input type="number" min="0" value="<?php echo $oneCost; ?>" step="0.01" name="oneCost" id="oneCost"></label> <br>
<label>Watt per Bulb: <input type="number" min="0.5" value="<?php echo $oneWatt; ?>" step="0.5" name="oneWatt" id="oneWatt"></label> <br>
<label>Bulb Lifespan (in hours): <input type="number" min="1" value="<?php echo $oneLifespan; ?>" name="oneLifespan" id="oneLifespan"></label> <br>

<br>
</td><td style="padding:1%;">

<h2>Bulb Two:  <span style="background-color: red;">&nbsp;&nbsp;&nbsp;</span></h2>

<label>Cost Per Bulb: <input type="number" min="0" value="<?php echo $twoCost; ?>" step="0.01" name="twoCost" id="twoCost"></label> <br>
<label>Watt per Bulb: <input type="number" min="0.5" value="<?php echo $twoWatt; ?>" step="0.5" name="twoWatt" id="twoWatt"></label> <br>
<label>Bulb Lifespan (in hours): <input type="number" min="1" value="<?php echo $twoLifespan; ?>" name="twoLifespan" id="twoLifespan"></label> <br>

</td></tr></table>

<!-- Start Navbar Script; Stolen from my old website; Yes, I made it myself. -->
<script type="text/javascript">
function VGC_toggle(id){
if (document.all){
if(document.all[id].style.display == 'none'){
document.all[id].style.display = '';
} else {
document.all[id].style.display = 'none';
}
return false;
} else if (document.getElementById){
if(document.getElementById(id).style.display == 'none'){
document.getElementById(id).style.display = 'block';
} else {
document.getElementById(id).style.display = 'none';
}
return false;
}
return false;
}
</script>
<!-- End Navbar Script; Stolen from my old website; Yes, I made it myself. -->

<script type="text/javascript"> <!-- Autofills forms with preset data -->
function autofill(bulb,cost,watts,lifespan){
document.getElementById(bulb + "Cost").value = cost;
document.getElementById(bulb + "Watt").value = watts;
document.getElementById(bulb + "Lifespan").value = lifespan;
return false;
}
</script>

<br><br>
<?php if($submitted){ echo '<div style="width:100%;text-align:center;"><h3 style="color:red;">Form Updated</h3></div>'; } ?>
<br><br>

<a href="#" onclick="VGC_toggle('tryThese'); return false;">Show/Hide Suggested Bulbs</a><br><br>
<div id="tryThese" style="display:none;">
<h2>Try These:</h2>
<h4>All 40 watt equivilant bulbs:</h4><br>

<table border="1" style="width:100%;">
<tr><td style="padding:1%;">
<b>Warning</b>: I haven't tested this bulb! Also note that the lifespan is lower and it has mixed reviews online!<br><br>
Name: <a href="http://www.lowes.com/pd_740059-43921-YGA03A41-A19-5.5W-83_1z0vvnnZ1z10fgwZ1z10ht5Z1z140qi__?productId=50423210&pl=1" target="_blank">UT LED 40W Equivilant Bulb 2-CT</a><br>
Available at: Lowe's Hardware<br>
Cost Per Bulb: $1.49<br>
Watts Per Bulb: 5.5<br>
Lifespan: 5,000 Hours<br>
<a href="#" onclick="autofill('one',1.49,5.5,5000);return false;">AutoFill Bulb 1</a> | <a href="#" onclick="autofill('two',1.49,5.5,5000);return false;">AutoFill Bulb 2</a><br>
</td><td style="padding:1%;">
Name: <b>OSRAM LED</b> (Not available online)<br>
Available at: Lowe's Hardware<br>
Cost Per Bulb: $6.98<br>
Watts Per Bulb: 6<br>
Lifespan: 25,000 Hours<br>
<a href="#" onclick="autofill('one',6.98,6,25000);return false;">AutoFill Bulb 1</a> | <a href="#" onclick="autofill('two',6.98,6,25000);return false;">AutoFill Bulb 2</a><br>
</td></tr><tr>
<td style="padding:1%;">
<b>Warning</b>: I haven't tested this one! OSRAM LED becomes cheaper than this one after 25 years.<br><br>
Name: <a href="http://www.walmart.com/ip/Great-Value-GVRLAO727D-Great-Value-LED-7W-Omni-Directional-A19-Dimmable-Light-Bulb/38591387" target="_blank">Great Value LED</a><br>
Available at: Walmart<br>
Cost Per Bulb: $4.84<br>
Watts Per Bulb: 7<br>
Lifespan: 24,900 Hours<br>
<a href="#" onclick="autofill('one',4.84,7,24900);return false;">Autofill Bulb 1</a> | <a href="#" onclick="autofill('two',4.84,7,24900);return false;">Autofill Bulb 2</a><br>
</td><td style="padding:1%;">
Name: <b>GE LED 40W @ 7W</b> (Not available online)<br>
Available at: Target<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(only slightly more expensive at Kroger's)<br>
Cost Per Bulb: $5.42<br>
Watts Per Bulb: 7<br>
Lifespan: 25,000 Hours<br>
<a href="#" onclick="autofill('one',5.42,7,25000);return false;">Autofill Bulb 1</a> | <a href="#" onclick="autofill('two',5.42,7,25000);return false;">Autofill Bulb 2</a><br>
</td></tr><tr><td style="padding:1%;">
Best CFL bulb I could find (40 watt equivilant):<br><br>
Name: <a href="http://www.lowes.com/pd_20928-75774-LBP9T227K_0__?productId=50189167" target="_blank">Utilitech 2-Pack Soft White CFL Bulb</a><br>
Available at: Lowe's Hardware<br>
Cost Per Bulb: $2.49<br>
Watts Per Bulb: 9<br>
Lifespan: 10,000 Hours<br>
<a href="#" onclick="autofill('one',2.49,9,10000);return false;">Autofill Bulb 1</a> | <a href="#" onclick="autofill('two',2.49,9,10000);return false;">Autofill Bulb 2</a><br>
</td><td style="padding:1%;">
Best Incandecent Bulb I could find (lowest watts used):<br><br>
Name: <a href="http://www.lowes.com/pd_76501-3-10015_0__?productId=1100567" target="_blank">SYLVANIA 2-Pack Soft White Incandescent Light Bulbs</a><br>
Available at: Lowe's Hardware<br>
Cost Per Bulb: $1.57<br>
Watts Per Bulb: 15<br>
Lifespan: 2,500 Hours (not a typo)<br>
<a href="#" onclick="autofill('one',1.57,15,2500);return false;">Autofill Bulb 1</a> | <a href="#" onclick="autofill('two',1.57,15,2500);return false;">Autofill Bulb 2</a><br>
</td></tr></table>
<br><br>

</div><!-- END tryThese -->

<div style="width:100%;text-align:center;">
<input type="submit" value="Submit" name="submit">
</div>
</form>

<br><br><hr style="width:95%;"><br><br>

<?php
if($submitted){ // IF the form was submitted
?>

<table style="width: 100%;background-color:lightgray;text-align:center;" border="1"><tr><td style="border: 1px solid black;">

<?php $oneInitialCost = $generalNumber * $oneCost; ?>
Bulb 1 Initial Cost: $<?php echo moneyFormat('%i', $oneInitialCost); ?><br>
Bulb 1 Total Watts: <?php echo $oneWatt * $generalNumber; ?><br>
Bulb 1 Power Used per Day (kWh/Day): <?php $onePowerPerDay = ($oneWatt*$generalNumber*$generalUse)/1000; echo $onePowerPerDay; ?><br>
Bulb 1 Power Cost per Year: $<?php $onePowerCostPerYear = ($onePowerPerDay * $generalCost * $generalNumber * 365); echo moneyFormat('%i', $onePowerCostPerYear); ?><br><br>

</td><td>
<br>
<?php $twoInitialCost = $generalNumber * $twoCost; ?>
Bulb 2 Initial Cost: $<?php echo moneyFormat('%i', $twoInitialCost); ?><br>
Bulb 2 Total Watts: <?php echo $twoWatt * $generalNumber; ?><br>
Bulb 2 Power Used per Day (kWh/Day): <?php $twoPowerPerDay = ($twoWatt*$generalNumber*$generalUse)/1000; echo $twoPowerPerDay; ?><br>
Bulb 2 Power Cost per Year: $<?php $twoPowerCostPerYear = ($twoPowerPerDay * $generalCost * $generalNumber * 365); echo moneyFormat('%i', $twoPowerCostPerYear); ?><br><br><br>

</td></tr></table>
<p>*All values below are added from the beginning to the listed year.</p>
<table border="1" style="width:100%;background-color:lightgray;text-align:center;">
<tr>
<td>Year</td><td>Bulb 1 Replacement Costs</td><td>Bulb 2 Replacement Costs</td><td>Bulb 1 Power Cost</td><td>Bulb 2 Power Cost</td><td style="background-color:black;">&nbsp;</td><td>Bulb 1 Total Cost</td><td>Bulb 2 Total Cost</td><td>Difference</td></tr>

<?php
$toggle = 1;

for($year=1; $year<=$generalYears; $year++){

if($toggle == 1){ $toggle = 0; }else{ $toggle = 1; }
?>
<tr style="background-color: <?php if($toggle==1){ echo 'lightgray'; }else{ echo '#969696'; } ?>">
<td><?php echo $year; ?></td>
<td><?php $oneReplacementCost = (floor(($year * 365 * $generalUse)/($oneLifespan + 24)) * $oneInitialCost) + ($oneInitialCost); echo moneyFormat('%i', $oneReplacementCost); ?></td>
<td><?php $twoReplacementCost = (floor(($year * 365 * $generalUse)/($twoLifespan + 24)) * $twoInitialCost) + ($twoInitialCost); echo moneyFormat('%i', $twoReplacementCost); ?></td>
<td><?php $onePowerCost = $onePowerPerDay * $generalNumber * $generalCost * ( $year * 365 ); echo moneyFormat('%i', $onePowerCost); ?></td>
<td><?php $twoPowerCost = $twoPowerPerDay * $generalNumber * $generalCost * ( $year * 365 ); echo moneyFormat('%i', $twoPowerCost); ?></td>

<?php

$oneTotalCost = $oneReplacementCost + $onePowerCost;
$twoTotalCost = $twoReplacementCost + $twoPowerCost;
$winner = 0;

if($oneTotalCost > $twoTotalCost){ $color = 'red'; $winner = 2; $diff = $oneTotalCost - $twoTotalCost; }
if($oneTotalCost < $twoTotalCost){ $color = 'blue'; $winner = 1; $diff = $twoTotalCost - $oneTotalCost; }
if($oneTotalCost == $twoTotalCost){ $color = 'black'; $winner = 0; $diff = 0; }

?>

<td style="background-color: <?php echo $color; ?>;">&nbsp;</td>
<td><?php echo moneyFormat('%i', $oneTotalCost); ?></td>
<td><?php echo moneyFormat('%i', $twoTotalCost); ?></td>
<td><?php if($winner != 0){?>
<span style="color: <?php echo $color; ?>;">Bulb <?php echo $winner; ?></span> is cheaper by &nbsp;$<?php echo moneyFormat('%i', $diff); ?>.&nbsp;
<?php }else{ ?>
No Difference.
<?php } ?>
</tr>
<?php
}
?>
</table>

<?php
} // END IF the form was submitted
?>

</body>
</html>
