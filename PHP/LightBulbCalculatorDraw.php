<?php

class LightBulbCalculatorDraw{

	private $calculator, $bulbOne, $bulbTwo, $yearsToChart, $moneyFormat;

	public function __construct($calculator = null, $bulbOne = null, $bulbTwo = null, $yearsToChart = null, $moneyFormat){
		$this->calculator = $calculator;
		$this->bulbOne = $bulbOne;
		$this->bulbTwo = $bulbTwo;
		$this->yearsToChart = (int)$yearsToChart;
		$this->moneyFormat = $moneyFormat;
	}

	public function sideBySide(){ // Start sideBySide
		$toDraw  = '<table id="sideBySide" border="1"><tr><td class="innerSide">' . "\n";
		$toDraw .= lang::bulbOneInitialCost . $this->moneyFormat($this->moneyFormat, $this->calculator->getReplacementCost( $this->bulbOne, 1 )) . "<br>\n";
		$toDraw .= lang::bulbOneTotalWatts . $this->bulbOne->getWatts() . "<br>\n";

		$bulbOnePowerPerDay = ($this->bulbOne->getWatts()*$this->calculator->getNumberOfBulbs()*$this->calculator->getHoursOfUse()) / 1000;
		$bulbOnePowerCostPerYear = ( $bulbOnePowerPerDay * $this->calculator->getCostOfPower() * $this->calculator->getNumberOfBulbs() * 365 );

		$toDraw .= lang::bulbOnePowerUsedPerDay . $bulbOnePowerPerDay . "<br>\n";
		$toDraw .= lang::bulbOnePowerCostPerYear . $this->moneyFormat($this->moneyFormat, $bulbOnePowerCostPerYear) . "<br><br>\n";
		$toDraw .= '</td><td>' . "\n";
		$toDraw .= '<br>' . "\n";


		$toDraw .= lang::bulbTwoInitialCost . $this->moneyFormat($this->moneyFormat, $this->calculator->getReplacementCost( $this->bulbTwo, 1 )) . "<br>\n";
		$toDraw .= lang::bulbTwoTotalWatts . $this->bulbTwo->getWatts() . "<br>\n";

		$bulbTwoPowerPerDay = ($this->bulbTwo->getWatts()*$this->calculator->getNumberOfBulbs()*$this->calculator->getHoursOfUse()) / 1000;
		$bulbTwoPowerCostPerYear = ( $bulbTwoPowerPerDay * $this->calculator->getCostOfPower() * $this->calculator->getNumberOfBulbs() * 365 );

		$toDraw .= lang::bulbTwoPowerUsedPerDay . $bulbTwoPowerPerDay . "<br>\n";
		$toDraw .= lang::bulbTwoPowerCostPerYear . $this->moneyFormat($this->moneyFormat, $bulbTwoPowerCostPerYear) . "<br><br><br>\n";
		$toDraw .= '</td></tr></table>' . "\n";

		return $toDraw;
	} // END sideBySide

	public function fullTable(){ // Start fullTable
		$toDraw  = '<table border="1" id="fullTable">' . "\n";
		$toDraw .= '<tr>' . "\n";
		$toDraw .= '<td>' . lang::year;
		$toDraw .= '</td><td>' . lang::bulbOneReplacementCost;
		$toDraw .= '</td><td>' . lang::bulbTwoReplacementCost;
		$toDraw .= '</td><td>' . lang::bulbOnePowerCost;
		$toDraw .= '</td><td>' . lang::bulbTwoPowerCost;
		$toDraw .= '</td><td class="noDifference">&nbsp;</td><td>' . lang::bulbOneTotalCost;
		$toDraw .= '</td><td>' . lang::bulbTwoTotalCost;
		$toDraw .= '</td><td>' . lang::difference . '</td></tr>';


			// Start Loop for every year to chart
			for($i = 1; $i <= $this->yearsToChart; $i++){

				// Alternate our style each loop:
				if($i % 2 != 0){
					$toDraw .= '<tr class="fullFirst"><td class="year">';
				}else{
					$toDraw .= '<tr class="fullSecond"><td class="year">';
				}

				$toDraw .= $i . '</td><td class="bulbOneReplacementCost">' . "\n";
				$toDraw .= $this->moneyFormat($this->moneyFormat, $this->calculator->getReplacementCost( $this->bulbOne, $i ));
				$toDraw .= '</td><td class="bulbTwoReplacementCost">' . "\n";
				$toDraw .= $this->moneyFormat($this->moneyFormat, $this->calculator->getReplacementCost( $this->bulbTwo, $i ));
				$toDraw .= '</td><td class="bulbOnePowerCost">' . "\n";
				$toDraw .= $this->moneyFormat($this->moneyFormat, $this->calculator->getPowerCost( $this->bulbOne, $i ));
				$toDraw .= '</td><td class="bulbTwoPowerCost">' . "\n";
				$toDraw .= $this->moneyFormat($this->moneyFormat, $this->calculator->getPowerCost( $this->bulbTwo, $i ));
				$toDraw .= '</td>';

				// Get which bulb is cheaper, returns -1 for bulb 1, 0 for no difference, and 1 for bulb 2
				$cheaperBulb = $this->calculator->compareBulbs( $this->bulbOne, $this->bulbTwo, $i );

				if($cheaperBulb < 0){
					$toDraw .= '<td class="bulbOne">&nbsp;</td>';
				}else{
					if($cheaperBulb == 0){
						$toDraw .= '<td class="noDifference">&nbsp;</td>';
					}else{
						$toDraw .= '<td class="bulbTwo">&nbsp;</td>';
					}
				}

				$toDraw .= '<td class="bulbOneTotalCost">' . "\n";
				$toDraw .= $this->moneyFormat($this->moneyFormat, $this->calculator->getTotalCost( $this->bulbOne, $i ));
				$toDraw .= '</td><td class="bulbTwoTotalCost">' . "\n";
				$toDraw .= $this->moneyFormat($this->moneyFormat, $this->calculator->getTotalCost( $this->bulbTwo, $i ));
				$toDraw .= '</td><td class="difference">' . "\n";

				if($cheaperBulb < 0){
                                        $toDraw .= lang::bulbOneCheaperBy . $this->moneyFormat($this->moneyFormat, $this->calculator->getDifference( $this->bulbOne, $this->bulbTwo, $i ));
                                }else{
                                        if($cheaperBulb == 0){
                                                $toDraw .= lang::noDifference;
                                        }else{
                                                $toDraw .= lang::bulbTwoCheaperBy . $this->moneyFormat($this->moneyFormat, $this->calculator->getDifference( $this->bulbTwo, $this->bulbOne, $i ));
                                        }
                                }

				$toDraw .= '</tr>';


			} // END Loop for every year to chart

		$toDraw .= '</table>';

		return $toDraw;
	} // END fullTable


	public function suggestedBulbs( $suggestedBulbs ){ // Start suggestedBulbs

		$toDraw = '<table id="suggestedBulbs"><tr class="trSuggestedBulb">' . "\n";
		$i = 1;

		foreach($suggestedBulbs as $item){ // Loop through the suggested bulbs
			if( $i == 3){
				$toDraw .= '<tr class="trSuggestedBulb"><td class="tdSuggestedBulb">' . "\n";
				$i = 1;
			}else{
				$toDraw .= '<td class="tdSuggestedBulb">' . "\n";
			}

			if(strlen($item['url']) > 4){
				$toDraw .= lang::suggestedName . '<a href="' . $item['url'] . '">' . $item['name'] . '</a>' . "<br>\n";
			}else{
				$toDraw .= lang::suggestedName . $item['name'] . "<br>\n";
			}
			$toDraw .= lang::suggestedAvailableAt . $item['availability'] . "<br>\n";
			$toDraw .= lang::suggestedCostPerBulb . $this->moneyFormat($this->moneyFormat, (double)$item['cost']) . "<br>\n";
			$toDraw .= lang::suggestedWattsPerBulb . (double)$item['watts'] . "<br>\n";
			$toDraw .= lang::suggestedLifespan . $item['lifespan'] . lang::suggestedHours . "<br>\n";
			$toDraw .= lang::suggestedNote . $item['note'] . "<br>\n";
			$toDraw .= '<a href="#" onclick="autofill(\'one\',' . $item['cost'] . ',' . $item['watts'] . ',' . $item['lifespan'] . ');return false;">' . lang::suggestedAutofillOne . '</a>';
			$toDraw .= ' | <a href="#" onclick="autofill(\'two\',' . $item['cost'] .',' . $item['watts'] . ',' . $item['lifespan'] . ');return false;">' . lang::suggestedAutofillTwo . '</a><br>' . "<br>\n";
			$toDraw .= '</td>' . "\n";

			if( $i == 2){
                                $toDraw .= '</tr>' . "\n";
                        }

			$i++;
		} // END Loop through suggested bulbs

		$toDraw .= '</table>';
		return $toDraw;
	} // END suggestedBulbs


	private function moneyFormat($format, $number){

	/*
		Formats money as US Dollars regardless of the type of server you are using.
		This needs to be updated for more money formats.
		This function is slightly modified code from a comment by Rafael M. Salvioni on:
		https://secure.php.net/manual/en/function.money-format.php
	*/

	setlocale(LC_MONETARY, 'en_US'); //just force the locale
	if(function_exists('money_format')){
		return money_format($format, $number);
	}else{
	    $regex  = '/%((?:[\^!\-]|\+|\(|\=.)*)([0-9]+)?'.'(?:#([0-9]+))?(?:\.([0-9]+))?([in%])/';
	    $locale = localeconv();
	    preg_match_all($regex, $format, $matches, PREG_SET_ORDER);
	    foreach ($matches as $fmatch) {
	        $value = floatval($number);
	        $flags = array(
	            'fillchar'  => preg_match('/\=(.)/', $fmatch[1], $match) ?
	                           $match[1] : ' ',
	            'nogroup'   => preg_match('/\^/', $fmatch[1]) > 0,
	            'usesignal' => preg_match('/\+|\(/', $fmatch[1], $match) ?
	                           $match[0] : '+',
	            'nosimbol'  => preg_match('/\!/', $fmatch[1]) > 0,
	            'isleft'    => preg_match('/\-/', $fmatch[1]) > 0
	        );
	        $width      = trim($fmatch[2]) ? (int)$fmatch[2] : 0;
	        $left       = trim($fmatch[3]) ? (int)$fmatch[3] : 0;
	        $right      = trim($fmatch[4]) ? (int)$fmatch[4] : $locale['int_frac_digits'];
	        $conversion = $fmatch[5];
	        $positive = true;
	        if ($value < 0) {
	            $positive = false;
	            $value  *= -1;
	        }
	        $letter = $positive ? 'p' : 'n';
	        $prefix = $suffix = $cprefix = $csuffix = $signal = '';
	        $signal = $positive ? $locale['positive_sign'] : $locale['negative_sign'];
	        switch (true) {
	            case $locale["{$letter}_sign_posn"] == 1 && $flags['usesignal'] == '+':
	                $prefix = $signal;
	                break;
	            case $locale["{$letter}_sign_posn"] == 2 && $flags['usesignal'] == '+':
	                $suffix = $signal;
	                break;
	            case $locale["{$letter}_sign_posn"] == 3 && $flags['usesignal'] == '+':
	                $cprefix = $signal;
	                break;
	            case $locale["{$letter}_sign_posn"] == 4 && $flags['usesignal'] == '+':
	                $csuffix = $signal;
	                break;
	            case $flags['usesignal'] == '(':
	            case $locale["{$letter}_sign_posn"] == 0:
	                $prefix = '(';
	                $suffix = ')';
	                break;
	        }
	        if (!$flags['nosimbol']) {
	            $currency = $cprefix .
	                        ($conversion == 'i' ? $locale['int_curr_symbol'] : $locale['currency_symbol']) .
	                        $csuffix;
	        } else {
	            $currency = '';
	        }
	        $space  = $locale["{$letter}_sep_by_space"] ? ' ' : '';
	        $value = number_format($value, $right, $locale['mon_decimal_point'],
	                 $flags['nogroup'] ? '' : $locale['mon_thousands_sep']);
	        $value = @explode($locale['mon_decimal_point'], $value);
	        $n = strlen($prefix) + strlen($currency) + strlen($value[0]);
	        if ($left > 0 && $left > $n) {
	            $value[0] = str_repeat($flags['fillchar'], $left - $n) . $value[0];
	        }
	        $value = implode($locale['mon_decimal_point'], $value);
	        if ($locale["{$letter}_cs_precedes"]) {
	            $value = $prefix . $currency . $space . $value . $suffix;
	        } else {
	            $value = $prefix . $value . $space . $currency . $suffix;
	        }
	        if ($width > 0) {
	            $value = str_pad($value, $width, $flags['fillchar'], $flags['isleft'] ?
	                     STR_PAD_RIGHT : STR_PAD_LEFT);
	        }
	        $format = str_replace($fmatch[0], $value, $format);
	    }
	    return $format;
	}
}

}
