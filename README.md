This is a simple cost calculator that compares two light bulbs. (Although it can do more than just light bulbs.) Right now it only
runs in PHP for webservers and in Java for local machines. However, I plan on making it for more languages.

I originally created it because I wanted to easily show people how big a difference a few watts makes in power costs. The easiest
 way to do that was to create a web script, which is why PHP was done first.


A future version will take our suggested bulbs and make it into a .xml or .json file. All of the versions of the calculator can
just load in that file. That way we only need to update one file whenever changes are made to the suggested bulbs.
